package cloudb.excelsior.com;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class fragment_startup_1 extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(
                R.layout.layout_fragment_startup_1, container, false);

        TextView app_name = (TextView) rootView.findViewById(R.id.app_name);
        Typeface type = Typeface.createFromAsset(getActivity().getAssets(),"fonts/Tellural Alt Bold.ttf");

        app_name.setText("Cloud Bucket");
        app_name.setTypeface(type);
        app_name.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 50);

        return rootView;
    }
}