package cloudb.excelsior.com;

import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ConcurrentModificationException;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class fragment_home_1 extends Fragment implements View.OnClickListener,LocationListener {

    TextView weather_console;
    ExecutorService executor = Executors.newFixedThreadPool(1);

    static final String API_KEY = "de93b22c4060749b1b819bd9b281c0e6";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(
                R.layout.layout_fragment_home_1, container, false);

        weather_console = (TextView) rootView.findViewById(R.id.output_weather);
        Button get_weather = (Button) rootView.findViewById(R.id.get_weather);
        get_weather.setOnClickListener(this);

        return rootView;
    }

    @Override
    public void onClick(View v) {

        String data="";

        // Get the location manager
        LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        // Define the criteria how to select the location provider -> use
        Criteria criteria = new Criteria();
        String provider = locationManager.getBestProvider(criteria, true);
        weather_console.setText(provider);
        locationManager.requestLocationUpdates(provider,400,1,this);
        Location location = locationManager.getLastKnownLocation(provider);

        while(location==null);

        Callable<String> getWeather = new getWeatherData(location.getLatitude(),location.getLongitude());
        Future<String> retrieve = executor.submit(getWeather);

        try {
            data=retrieve.get();
            weather_console.setText(data);
        }catch(InterruptedException e)  {
            e.printStackTrace();
        }catch (ExecutionException e){
            e.printStackTrace();
        }

        try {
            JSONObject obj = new JSONObject(data);
            JSONObject coorObj = obj.getJSONObject("coord");
            //weather_console.setText(location.getLongitude()+"");
        } catch(JSONException e) {
            e.printStackTrace();
        }


    }

    private class getWeatherData implements Callable<String> {

        double lat,lon;

        private getWeatherData(double lat, double lon)    {

            this.lat=lat;
            this.lon=lon;

        }

        public String call() {

            HttpURLConnection con = null;
            InputStream is = null;

            try

            {
                con = (HttpURLConnection) (new URL("http://api.openweathermap.org/data/2.5/weather?lat="+lat+"&lon="+lon)).openConnection();
                con.setRequestMethod("GET");
                con.setDoInput(true);
                con.setDoOutput(true);
                con.connect();

                // Let's read the response
                StringBuffer buffer = new StringBuffer();
                is = con.getInputStream();
                BufferedReader br = new BufferedReader(new InputStreamReader(is));
                String line = null;
                while ((line = br.readLine()) != null)
                    buffer.append(line + "\r\n");

                is.close();
                con.disconnect();
                return buffer.toString();
            } catch (
                    Throwable t
                    )

            {
                t.printStackTrace();
            } finally

            {
                try {
                    is.close();
                } catch (Throwable t) {
                }
                try {
                    con.disconnect();
                } catch (Throwable t) {
                }
            }

            return null;
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        int lat = (int) (location.getLatitude());
        int lng = (int) (location.getLongitude());
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onProviderEnabled(String provider) {
        Toast.makeText(getActivity(), "Enabled new provider " + provider,
                Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onProviderDisabled(String provider) {
        Toast.makeText(getActivity(), "Disabled provider " + provider,
                Toast.LENGTH_SHORT).show();
    }
}
